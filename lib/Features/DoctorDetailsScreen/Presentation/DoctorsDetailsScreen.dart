import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:viswals_doctor_details/Features/DoctorDetailsScreen/Controller/DoctorProvider.dart';
import 'package:viswals_doctor_details/Features/DoctorDetailsScreen/Presentation/Widgets/ExpandedAppBar.dart';
import 'package:viswals_doctor_details/Features/DoctorDetailsScreen/Presentation/Widgets/DetailsTabBarView.dart';
import 'package:viswals_doctor_details/Features/DoctorDetailsScreen/Presentation/Widgets/SliverTabBarDelegate.dart';

class DoctorDetailsScreen extends StatefulWidget {
  const DoctorDetailsScreen({super.key});

  @override
  State<DoctorDetailsScreen> createState() => _DoctorDetailsScreenState();
}

class _DoctorDetailsScreenState extends State<DoctorDetailsScreen>
    with TickerProviderStateMixin {
  @override
  void initState() {
    context.read<DoctorProvider>().tabController =
        TabController(length: TabsEnum.values.length, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final doctorProvider = context.watch<DoctorProvider>();
    return Scaffold(
      backgroundColor: doctorProvider.currentUserLevel.styles.color,
      body: SafeArea(
        bottom: false,
        top: false,
        child: CustomScrollView(
          physics: const ClampingScrollPhysics(),
          slivers: [
            // Expanded and collapsed Appbar
            SliverAppBar(
                // toolbarHeight: 200.h,
                expandedHeight: 458.h, // Height when fully expanded
                pinned: true, // Keep the app bar pinned to the top
                floating:
                    true, // Make the app bar snap into place when scrolling up
                flexibleSpace: const FlexibleSpaceBar(
                  background: ExpandedAppBar(),
                )),
            // Availibility button and Tab bar
            // They will stick at top and won't scroll
            SliverPersistentHeader(
              delegate: SliverTabBarDelegate(
                  context.read<DoctorProvider>().tabController),
              pinned: true,
            ),
            // Tab bar view, that shows widgets of selected tab
            // user can swipe or click on tab to select another one
            const SliverFillRemaining(
              hasScrollBody: true,
              child: DetailsTabBarView(),
            )
          ],
        ),
      ),
    );
  }
}
