import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:viswals_doctor_details/Core/Utils/AppAssests.dart';
import 'package:viswals_doctor_details/Core/Utils/AppColors.dart';
import 'package:viswals_doctor_details/Core/Utils/AppFontFamily.dart';
import 'package:viswals_doctor_details/Core/Widgets/AppTextWidget.dart';
import 'package:viswals_doctor_details/Features/DoctorDetailsScreen/Presentation/Widgets/AddToFavouriteButton.dart';

class SelectUserSkinWidget extends StatelessWidget {
  const SelectUserSkinWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      height: 44.h,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          10.horizontalSpace,
          // back arrow
          Container(
            height: 28.h,
            width: 28.h,
            padding: EdgeInsets.symmetric(vertical: 8.h),
            child: SvgPicture.asset(
              AppAssets.backArrow,
              color: AppColors.divColor,
            ),
          ),
          // drop down button to select skin
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                18.horizontalSpace,
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 14.h),
                  child: SizedBox(
                    height: 16.h,
                    child: const AppTextWidget(
                      letterSpacing: 0.17,
                      txtTitle: "Aesthetic Dentistry",
                      fontSize: 11,
                      txtColor: AppColors.divColor,
                      fontFamily: AppFontFamily.poppins,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                8.horizontalSpace,
                Padding(
                  padding: EdgeInsets.only(top: 20.h, bottom: 18.h),
                  child: SvgPicture.asset(
                    AppAssets.dropdownArrow,
                    width: 10.w,
                    height: 6.h,
                    color: AppColors.divColor,
                  ),
                )
              ],
            ),
          ),
          // add to favourite button
          Padding(
            padding: EdgeInsets.symmetric(vertical: 12.9.h),
            child: const AddToFavouriteButton(),
          )
        ],
      ),
    );
  }
}
