import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:viswals_doctor_details/Features/DoctorDetailsScreen/Controller/DoctorProvider.dart';
import 'package:viswals_doctor_details/Features/DoctorDetailsScreen/Presentation/Widgets/AwardsDetailsWidget.dart';
import 'package:viswals_doctor_details/Features/DoctorDetailsScreen/Presentation/Widgets/BioDetailsWidget.dart';
import 'package:viswals_doctor_details/Features/DoctorDetailsScreen/Presentation/Widgets/LocationDetailWidget.dart';
import 'package:viswals_doctor_details/Features/DoctorDetailsScreen/Presentation/Widgets/PerformanceDetailsWidget.dart';
import 'package:viswals_doctor_details/Features/DoctorDetailsScreen/Presentation/Widgets/PricesDetailsWidget.dart';
import 'package:viswals_doctor_details/Features/DoctorDetailsScreen/Presentation/Widgets/SkinDetailsWidget.dart';

// ignore: constant_identifier_names
enum TabsEnum { Bio, Skin, Performance, Awards, Prices, Locations }

class DetailsTabBarView extends StatefulWidget {
  const DetailsTabBarView({super.key});

  @override
  State<DetailsTabBarView> createState() => _DetailsTabBarViewState();
}

class _DetailsTabBarViewState extends State<DetailsTabBarView>
    with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return TabBarView(
        controller: context.read<DoctorProvider>().tabController,
        children: const [
          BioDetailsWidget(),
          SkinDetailsWidget(),
          PerformanceDetailsWidget(),
          AwardsDetailsWidget(),
          PricesDetailsWidget(),
          LocationDetailsWidget(),
        ]);
  }
}
