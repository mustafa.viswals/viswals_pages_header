import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:viswals_doctor_details/Core/Utils/AppAssests.dart';
import 'package:viswals_doctor_details/Core/Utils/AppColors.dart';

class AddToFavouriteButton extends StatefulWidget {
  const AddToFavouriteButton({super.key});

  @override
  _AddToFavouriteButtonState createState() => _AddToFavouriteButtonState();
}

class _AddToFavouriteButtonState extends State<AddToFavouriteButton>
    with TickerProviderStateMixin {
  bool _isLiked = false;

  void _toggleLike() {
    setState(() {
      _isLiked = !_isLiked;
    });
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: Colors.transparent,
      onTap: _toggleLike,
      child: SizedBox(
        height: 20.h,
        width: 20.h,
        child: Visibility(
          visible: !_isLiked,
          replacement: SvgPicture.asset(
            AppAssets.heartFill,
            height: 18.h,
            width: 20.h,
            color: AppColors.divColor,
          ),
          child: SvgPicture.asset(
            AppAssets.heart,
            height: 18.h,
            width: 20.h,
            color: AppColors.divColor,
          ),
        ),
      ),
    );
  }
}
