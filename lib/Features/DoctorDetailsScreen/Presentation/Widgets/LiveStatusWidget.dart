import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:viswals_doctor_details/Core/Utils/AppAssests.dart';
import 'package:viswals_doctor_details/Core/Utils/AppColors.dart';
import 'package:viswals_doctor_details/Core/Widgets/AppTextWidget.dart';
import 'package:viswals_doctor_details/Features/DoctorDetailsScreen/Controller/DoctorProvider.dart';
// Live status of user

// enum of status
enum DoctorLiveStatus { live, busy, none }

class LiveStatusWidget extends StatelessWidget {
  const LiveStatusWidget({super.key, required this.status});
  final DoctorLiveStatus status;
  @override
  Widget build(BuildContext context) {
    return Consumer<DoctorProvider>(builder: (context, prov, _) {
      return Visibility(
        visible: prov.status != DoctorLiveStatus.none,
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5.r), color: Colors.black),
          alignment: Alignment.center,
          height: 24.h,
          padding: EdgeInsets.symmetric(horizontal: 8.w),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Visibility(
                visible: DoctorLiveStatus.live == prov.status,
                replacement: SvgPicture.asset(
                  AppAssets.busy,
                  color: prov.currentUserLevel.styles.color,
                ),
                child: SvgPicture.asset(
                  AppAssets.videoCam,
                  color: AppColors.live,
                ),
              ),
              2.horizontalSpace,
              AppTextWidget(
                txtColor: DoctorLiveStatus.busy == prov.status
                    ? AppColors.white
                    : AppColors.live,
                txtTitle:
                    DoctorLiveStatus.busy == prov.status ? "BUSY" : "LIVE NOW",
                fontSize: 9,
              ),
            ],
          ),
        ),
      );
    });
  }
}
