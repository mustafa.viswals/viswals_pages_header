import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:viswals_doctor_details/Core/Utils/AppAssests.dart';
import 'package:viswals_doctor_details/Core/Utils/AppColors.dart';
import 'package:viswals_doctor_details/Core/Widgets/AppTextWidget.dart';
import 'package:viswals_doctor_details/Features/DoctorDetailsScreen/Controller/DoctorProvider.dart';
// View Availability button

class ViewAvailabilityButton extends StatelessWidget {
  const ViewAvailabilityButton({super.key});

  @override
  Widget build(BuildContext context) {
    return Consumer<DoctorProvider>(builder: (context, docProv, _) {
      return Container(
        margin: EdgeInsets.symmetric(horizontal: 20.w, vertical: 20.h),
        // padding: EdgeInsets.symmetric(vertical: 16.h),
        height: 48.h,
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: AppColors.whitef5,
            borderRadius: BorderRadius.circular(10.r)),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const AppTextWidget(
              letterSpacing: 0,
              height: 20,
              txtTitle: "VIEW AVAILABILITY",
              txtColor: AppColors.textBlack,
              fontWeight: FontWeight.w800,
            ),
            8.horizontalSpace,
            SvgPicture.asset(
              AppAssets.arrowRight,
              color: docProv.currentUserLevel.styles.color,
            )
          ],
        ),
      );
    });
  }
}
