import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:viswals_doctor_details/Core/Utils/AppAssests.dart';
import 'package:viswals_doctor_details/Core/Utils/AppColors.dart';
import 'package:viswals_doctor_details/Core/Utils/AppFontFamily.dart';
import 'package:viswals_doctor_details/Core/Widgets/AppTextWidget.dart';
import 'package:viswals_doctor_details/Features/DoctorDetailsScreen/Controller/DoctorProvider.dart';

// Displays user name and level of user's currently selected skin
class NameAndLevelWidget extends StatelessWidget {
  const NameAndLevelWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 20.w),
      child: Row(
        children: [
          Expanded(
            child: Row(
              children: [
                Container(
                  height: 24.h,
                  alignment: Alignment.centerLeft,
                  color: AppColors.white,
                  child: const Flexible(
                    child: AppTextWidget(
                      txtTitle: "Dr. Helen Rosemary",
                      maxLine: 1,
                      fontSize: 17,
                      letterSpacing: 0,
                      txtColor: AppColors.divColor,
                      fontFamily: AppFontFamily.poppins,
                      fontWeight: FontWeight.w900,
                    ),
                  ),
                ),
                8.horizontalSpace,
                SvgPicture.asset(
                  AppAssets.verified,
                  height: 16.h,
                  width: 16.h,
                  color: AppColors.divColor,
                ),
              ],
            ),
          ),
          InkWell(
            onTap: () {
              context.read<DoctorProvider>().switchUserLevel();
            },
            child: Container(
                height: 24.h,
                alignment: Alignment.center,
                padding: EdgeInsets.only(
                  right: 8.w,
                  left: 6.w,
                ),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5.r),
                    color: AppColors.divColor),
                child: Consumer<DoctorProvider>(
                    builder: (context, doctorProvider, _) {
                  return Row(mainAxisSize: MainAxisSize.min, children: [
                    AppTextWidget(
                        txtTitle: doctorProvider.currentUserLevel.styles.name,
                        fontSize: 11,
                        fontWeight: FontWeight.w800,
                        letterSpacing: 0,
                        txtColor: AppColors.divColorLight
                        // txtColor: doctorProvider.currentUserLevel.styles.color,
                        ),
                    4.horizontalSpace,
                    ListView.builder(
                        itemCount:
                            doctorProvider.currentUserLevel.styles.numberOfHats,
                        padding: EdgeInsets.zero,
                        scrollDirection: Axis.horizontal,
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemBuilder: (context, index) {
                          return Padding(
                            padding: EdgeInsets.only(right: 2.w),
                            child: Container(
                              height: 12.h,
                              width: 12.h,
                              // alignment: Alignment.center,
                              padding: EdgeInsets.symmetric(vertical: 1.52.h),
                              child: SvgPicture.asset(AppAssets.levelIcon,
                                  color: AppColors.divColorLight
                                  // doctorProvider.currentUserLevel.styles.color,
                                  ),
                            ),
                          );
                        }),
                  ]);
                })),
          )
        ],
      ),
    );
  }
}
