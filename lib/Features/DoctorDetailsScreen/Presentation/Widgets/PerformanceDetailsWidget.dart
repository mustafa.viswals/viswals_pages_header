import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:viswals_doctor_details/Core/Utils/AppColors.dart';
import 'package:viswals_doctor_details/Core/Utils/AppFontFamily.dart';
import 'package:viswals_doctor_details/Core/Widgets/AppTextWidget.dart';
// Tab bar view widget for performace tab
class PerformanceDetailsWidget extends StatelessWidget {
  const PerformanceDetailsWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: AppColors.white,
      ),
      padding: EdgeInsets.symmetric(horizontal: 20.w),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          32.verticalSpace,
          const AppTextWidget(
            txtTitle: "My Digital Avatar",
            txtColor: AppColors.textBlack,
            fontSize: 15,
            fontWeight: FontWeight.w800,
            fontFamily: AppFontFamily.poppins,
          )
        ],
      ),
    );
  }
}
