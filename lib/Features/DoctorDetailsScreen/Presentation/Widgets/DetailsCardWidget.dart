import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:viswals_doctor_details/Core/Utils/AppAssests.dart';
import 'package:viswals_doctor_details/Core/Utils/AppColors.dart';
import 'package:viswals_doctor_details/Core/Utils/AppFontFamily.dart';
import 'package:viswals_doctor_details/Core/Widgets/AppTextWidget.dart';

class DetailsCardListWidget extends StatelessWidget {
  const DetailsCardListWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 20.w),
      child: const Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          _DetailsCardWidget(
            icon: AppAssets.skins,
            value: "3",
            lable: "Skins",
          ),
          _DetailsCardWidget(
            icon: AppAssets.appointment,
            value: "5",
            lable: "Appointments",
          ),
          _DetailsCardWidget(
            icon: AppAssets.performace,
            value: "4.95",
            lable: "Performance",
          ),
          _DetailsCardWidget(
            icon: AppAssets.careerAwards,
            value: "20",
            lable: "Career Awards",
          ),
          _DetailsCardWidget(
            icon: AppAssets.accolades,
            value: "3",
            lable: "Accolades",
          ),
        ],
      ),
    );
  }
}

class _DetailsCardWidget extends StatelessWidget {
  const _DetailsCardWidget({
    required this.icon,
    required this.value,
    required this.lable,
  });
  final String icon;
  final String value;
  final String lable;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 96.h,
      width: 62.w,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            alignment: Alignment.center,
            height: 75.h,
            padding: EdgeInsets.only(top: 14.h, bottom: 8.5.h),
            decoration: BoxDecoration(
                color: Colors.white.withOpacity(0.15),
                borderRadius: BorderRadius.circular(10.r)),
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.center,
              // mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  height: 20.r,
                  width: 20.r,
                  color: AppColors.divColorMed,
                  child: SvgPicture.asset(
                    icon,
                    height: 20.h,
                    width: 20.h,
                    color: AppColors.white,
                  ),
                ),
                // const Spacer(),
                12.verticalSpace,
                Container(
                  alignment: Alignment.center,
                  color: AppColors.divColorMed,
                  height: 15.h,
                  child: AppTextWidget(
                    txtTitle: value,
                    fontSize: 15,
                    textAlign: TextAlign.center,
                    fontWeight: FontWeight.bold,
                  ),
                )
              ],
            ),
          ),
          8.verticalSpace,
          AppTextWidget(
            txtTitle: lable,
            letterSpacing: -0.18,
            fontSize: 9,
            fontFamily: AppFontFamily.oxanium,
          )
        ],
      ),
    );
  }
}
