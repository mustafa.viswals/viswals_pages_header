import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:viswals_doctor_details/Core/Utils/AppColors.dart';
import 'package:viswals_doctor_details/Core/Utils/AppFontFamily.dart';
import 'package:viswals_doctor_details/Features/DoctorDetailsScreen/Controller/DoctorProvider.dart';
import 'package:viswals_doctor_details/Features/DoctorDetailsScreen/Presentation/Widgets/DetailsTabBarView.dart';
import 'package:viswals_doctor_details/Features/DoctorDetailsScreen/Presentation/Widgets/ViewAvailabilityButton.dart';

// A custom SliverDelegate to fix the View Availabitily button and Tab bar on top and making tab bar view scrollable
class SliverTabBarDelegate extends SliverPersistentHeaderDelegate {
  SliverTabBarDelegate(this._tabController);

  final TabController _tabController;

  @override
  double get minExtent => 128.h;
  @override
  double get maxExtent => 129.h;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Container(
      decoration: BoxDecoration(
          color: AppColors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(16.r), topRight: Radius.circular(16.r))),
      child: Column(
        children: [
          const ViewAvailabilityButton(),
          8.verticalSpace,
          Padding(
            padding: EdgeInsets.only(left: 8.w, right: 8.w),
            child: Consumer<DoctorProvider>(builder: (context, docProv, _) {
              return TabBar(
                  isScrollable: true,
                  indicatorWeight: 2,
                  indicator: BoxDecoration(
                      border: Border(
                          bottom: BorderSide(
                              color: docProv.currentUserLevel.styles.color,
                              width: 2.h))),
                  dividerColor: Colors.transparent,
                  labelPadding:
                      EdgeInsets.only(right: 12.w, left: 12.w, bottom: 8.h),
                  indicatorColor: docProv.currentUserLevel.styles.color,
                  labelColor: docProv.currentUserLevel.styles.color,
                  labelStyle: const TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                  unselectedLabelColor: AppColors.textBlack,
                  unselectedLabelStyle: const TextStyle(
                    fontWeight: FontWeight.w400,
                  ),
                  controller: _tabController,
                  tabs: TabsEnum.values
                      .map((e) => Text(
                            e.name,
                            style: TextStyle(
                                fontSize: 15.sp,
                                height: 15.h / 15.sp,
                                fontFamily: AppFontFamily.oxanium.family),
                          ))
                      .toList());
            }),
          ),
        ],
      ),
    );
  }

  @override
  bool shouldRebuild(SliverTabBarDelegate oldDelegate) {
    return false;
  }
}
