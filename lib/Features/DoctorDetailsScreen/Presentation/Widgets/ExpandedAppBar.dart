import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:viswals_doctor_details/Core/Utils/AppAssests.dart';
import 'package:viswals_doctor_details/Core/Utils/AppColors.dart';
import 'package:viswals_doctor_details/Features/DoctorDetailsScreen/Controller/DoctorProvider.dart';
import 'package:viswals_doctor_details/Features/DoctorDetailsScreen/Presentation/Widgets/DetailsCardWidget.dart';
import 'package:viswals_doctor_details/Features/DoctorDetailsScreen/Presentation/Widgets/NameAndLevelWidget.dart';
import 'package:viswals_doctor_details/Features/DoctorDetailsScreen/Presentation/Widgets/SelectUserSkinWidget.dart';
import 'package:viswals_doctor_details/Features/DoctorDetailsScreen/Presentation/Widgets/TopicAndStatusWidget.dart';

// Expanded app bar, where all the details of user are shown
class ExpandedAppBar extends StatelessWidget {
  const ExpandedAppBar({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Consumer<DoctorProvider>(builder: (context, prov, _) {
      return Container(
        // color: prov.currentUserLevel.styles.color,
        color: const Color(0xffFAD4D4),
        padding: EdgeInsets.only(right: 20.w),
        child: Column(
          children: [
            44.verticalSpace,
            const SelectUserSkinWidget(),
            14.verticalSpace,
            // User profile pic
            InkWell(
              onTap: () {
                context.read<DoctorProvider>().switchDoctorStatus();
              },
              child: Container(
                height: 186.h,
                width: 186.h,
                margin: EdgeInsets.only(left: 20.w),
                padding: EdgeInsets.all(4.w),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(color: AppColors.divColor, width: 4.w)),
                child: const CircleAvatar(
                    backgroundImage: AssetImage(
                  AppAssets.profilePic,
                )),
              ),
            ),
            30.verticalSpace,
            // User name and level of skin user is of
            const NameAndLevelWidget(),
            8.verticalSpace,
            // Title of user and current status(Live/Busy/Not Live)
            //also shows a tag that represent if use is in top 100
            const TopicAndStatusWidget(),
            16.verticalSpace,
            // Horizontal list of cards with some details about user
            // no. of skins, no. appointments, performace, no. of career awards etc
            const DetailsCardListWidget(),
            16.verticalSpace
          ],
        ),
      );
    });
  }
}
