import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:viswals_doctor_details/Core/Utils/AppColors.dart';
import 'package:viswals_doctor_details/Core/Widgets/AppTextWidget.dart';
import 'package:viswals_doctor_details/Features/DoctorDetailsScreen/Presentation/Widgets/LiveStatusWidget.dart';

// Topic and current status of user
class TopicAndStatusWidget extends StatelessWidget {
  const TopicAndStatusWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 20.w),
      child: Row(
        children: [
          Container(
              alignment: Alignment.centerLeft,
              height: 13.h,
              color: AppColors.white,
              child: const AppTextWidget(
                txtTitle: "Smile Designer",
                txtColor: AppColors.divColor,
                letterSpacing: 0,
              )),
          const Spacer(),
          const LiveStatusWidget(
            status: DoctorLiveStatus.live,
          ),
          8.horizontalSpace,
          Container(
            padding: EdgeInsets.symmetric(horizontal: 8.w),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5.r),
                border: Border.all(color: AppColors.divColor)),
            height: 24.h,
            alignment: Alignment.center,
            child: const AppTextWidget(
              txtColor: AppColors.divColor,
              txtTitle: "UK TOP 100",
              fontSize: 11,
            ),
          ),
        ],
      ),
    );
  }
}
