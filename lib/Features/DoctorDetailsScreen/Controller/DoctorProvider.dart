import 'package:flutter/material.dart';
import 'package:viswals_doctor_details/Core/Utils/UserLevelsEnum.dart';
import 'package:viswals_doctor_details/Features/DoctorDetailsScreen/Presentation/Widgets/LiveStatusWidget.dart';

class DoctorProvider extends ChangeNotifier {
  UserLevels currentUserLevel = UserLevels.elite;
  DoctorLiveStatus status = DoctorLiveStatus.none;

  late TabController tabController; // initialized in DoctorsDetailsScreen

  /// switchUserLevel is just for testing purpose. It help us to see if all colros are changing as per user level
  void switchUserLevel() {
    int index = UserLevels.values.indexOf(currentUserLevel) + 1;
    if (index >= UserLevels.values.length) {
      index = 0;
    }
    currentUserLevel = UserLevels.values[index];
    notifyListeners();
  }

  void switchDoctorStatus() {
    int index = DoctorLiveStatus.values.indexOf(status) + 1;
    if (index >= DoctorLiveStatus.values.length) {
      index = 0;
    }
    status = DoctorLiveStatus.values[index];
    notifyListeners();
  }
}
