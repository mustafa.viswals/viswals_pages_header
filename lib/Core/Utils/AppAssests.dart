class AppAssets {
  static const String _imagesBasePath = "assets/images";
  static const String _iconsBasePath = "assets/icons";
  static const String profilePic = "$_imagesBasePath/profile.png";
  static const String skins = "$_iconsBasePath/skins.svg";
  static const String appointment = "$_iconsBasePath/appointments.svg";
  static const String careerAwards = "$_iconsBasePath/careerAwards.svg";
  static const String accolades = "$_iconsBasePath/accolades.svg";
  static const String performace = "$_iconsBasePath/performance.svg";
  static const String videoCam = "$_iconsBasePath/videoCam.svg";
  static const String levelIcon = "$_iconsBasePath/levelIcon.svg";

  static const String verified = "$_iconsBasePath/recommended.svg";
  static const String busy = "$_iconsBasePath/busy.svg";
  static const String arrowRight = "$_iconsBasePath/arrowRight.svg";
  static const String dropdownArrow = "$_iconsBasePath/dropdownArrow.svg";
  static const String verifyTick = "$_iconsBasePath/verifyTick.svg";
  static const String backArrow = "$_iconsBasePath/backArrow.svg";
  static const String heartFill = "$_iconsBasePath/HeartFill.svg";
  static const String heart = "$_iconsBasePath/Heart.svg";
}
