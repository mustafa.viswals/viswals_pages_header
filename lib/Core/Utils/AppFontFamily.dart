enum AppFontFamily {
  poppins("Poppins"),
  oxanium("Oxanium");

  final String family;
  const AppFontFamily(this.family);
}
