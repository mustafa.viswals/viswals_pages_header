import 'package:flutter/material.dart';
import 'package:viswals_doctor_details/Core/Utils/AppColors.dart';

// Enum of user levels,
// Added an object with each enum value that stores 
// color, number of hats to display and label to show for a level on details screen
enum UserLevels {
  graduate(UserLevelStyles(color: AppColors.graduate, numberOfHats: 1,name: "GRADUATE")),
  advance(UserLevelStyles(color: AppColors.advanced, numberOfHats: 2,name: "ADVANCED")),
  master(UserLevelStyles(color: AppColors.master, numberOfHats: 3,name: "MASTER")),
  pro(UserLevelStyles(color: AppColors.pro, numberOfHats: 4,name: "PRO")),
  elite(UserLevelStyles(color: AppColors.elite, numberOfHats: 5,name: "ELITE"));

  final UserLevelStyles styles;
  const UserLevels(this.styles);
}

class UserLevelStyles {
  final Color color; // Color to show in details screen for a level
  final int numberOfHats; // number hats that represent a user level, lower the level less the hats
  final String name; // Name of level 

  const UserLevelStyles({required this.color, required this.numberOfHats,required this.name});
}