import 'package:flutter/material.dart';

class AppColors {
  static const Color live = Color(0xff2EFD94);

  static const Color white = Color(0xffffffff);
  static const Color elite = Color(0xff1278ED);
  static const Color pro = Color(0xffFF6452);
  static const Color red = Color(0xffFF0000);
  
  static const Color master = Color(0xffF7C03C);
  static const Color advanced = Color(0xff0FE3D1);
  static const Color graduate = Color(0xff9BBCCB);

  static const Color black = Color(0xff000000);
  static const Color whitef5 = Color(0xffF5F5F5);
  static const Color textBlack = Color(0xff242426);

  static const Color divColor = Color(0xffFF0000);
  static const Color divColorMed = Color(0xffFF6F6F);
  static const Color divColorLight = Color(0xffFAD4D4);
}
