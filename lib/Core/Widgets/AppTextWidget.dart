import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:viswals_doctor_details/Core/Utils/AppFontFamily.dart';

/// common App Text widget

class AppTextWidget extends StatelessWidget {
  const AppTextWidget(
      {Key? key,
      required this.txtTitle,
      this.txtColor = Colors.white,
      this.fontWeight = FontWeight.w400,
      this.fontStyle = FontStyle.normal,
      this.fontSize = 13,
      this.maxLine = 10,
      this.textAlign = TextAlign.left,
      this.overflow = TextOverflow.ellipsis,
      this.decoration = TextDecoration.none,
      this.fontFamily,
      this.height,
      this.letterSpacing = 0,
      this.shadows})
      : super(key: key);

  final String? txtTitle;
  final Color txtColor;
  final FontWeight fontWeight;
  final FontStyle fontStyle;
  final double fontSize;
  final int maxLine;
  final TextAlign textAlign;
  final TextOverflow overflow;
  final AppFontFamily? fontFamily;
  final TextDecoration decoration;
  final List<Shadow>? shadows;
  final double? letterSpacing;
  final double? height;
  @override
  Widget build(BuildContext context) {
    return Text(txtTitle ?? "N/A",
        maxLines: maxLine,
        overflow: overflow,
        textAlign: textAlign,
        style: TextStyle(
            letterSpacing: letterSpacing,
            fontFamily: fontFamily?.family,
            color: txtColor,
            fontWeight: fontWeight,
            fontStyle: fontStyle,
            fontSize: fontSize.sp,
            decoration: decoration,
            shadows: shadows ?? []));
  }
}
