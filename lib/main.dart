import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:viswals_doctor_details/Core/Utils/AppFontFamily.dart';
import 'package:viswals_doctor_details/Features/DoctorDetailsScreen/Controller/DoctorProvider.dart';
import 'package:viswals_doctor_details/Features/DoctorDetailsScreen/Presentation/DoctorsDetailsScreen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<DoctorProvider>(create: (_) => DoctorProvider()),
      ],
      child: ScreenUtilInit(
        designSize: const Size(414, 896),
        ensureScreenSize: true,
        child: MaterialApp(
          title: 'Doctor',
          theme: ThemeData(
            fontFamily: AppFontFamily.oxanium.family,
            useMaterial3: true,
          ),
          home: const DoctorDetailsScreen(),
        ),
      ),
    );
  }
}
